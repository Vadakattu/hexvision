import numpy as np
import os


def download_model(basedir = os.getcwd()):
    # create folder for resources
    modeldir = os.path.join(basedir, 'resources')
    if not os.access(modeldir, os.W_OK):
        os.mkdir(modeldir)

    # prepare all the labels
    # scene category relevant
    file_name_category = os.path.join(modeldir, 'categories_places365.txt')
    if not os.access(file_name_category, os.W_OK):
        synset_url = 'https://raw.githubusercontent.com/csailvision/places365/master/categories_places365.txt'
        os.system('wget ' + synset_url + ' -P ' + modeldir)
    classes = list()
    with open(file_name_category) as class_file:
        for line in class_file:
            classes.append(line.strip().split(' ')[0][3:])
    classes = tuple(classes)

    # indoor and outdoor relevant
    file_name_IO = os.path.join(modeldir, 'IO_places365.txt')
    if not os.access(file_name_IO, os.W_OK):
        synset_url = 'https://raw.githubusercontent.com/csailvision/places365/master/IO_places365.txt'
        os.system('wget ' + synset_url + ' -P ' + modeldir)
    with open(file_name_IO) as f:
        lines = f.readlines()
        labels_IO = []
        for line in lines:
            items = line.rstrip().split()
            labels_IO.append(int(items[-1]) - 1)  # 0 is indoor, 1 is outdoor
    labels_IO = np.array(labels_IO)

    # scene attribute relevant
    file_name_attribute = os.path.join(modeldir, 'labels_sunattribute.txt')
    if not os.access(file_name_attribute, os.W_OK):
        synset_url = 'https://raw.githubusercontent.com/csailvision/places365/master/labels_sunattribute.txt'
        os.system('wget ' + synset_url + ' -P ' + modeldir)
    with open(file_name_attribute) as f:
        lines = f.readlines()
        labels_attribute = [item.rstrip() for item in lines]
    file_name_W = os.path.join(modeldir, 'W_sceneattribute_wideresnet18.npy')
    if not os.access(file_name_W, os.W_OK):
        synset_url = 'http://places2.csail.mit.edu/models_places365/W_sceneattribute_wideresnet18.npy'
        os.system('wget ' + synset_url + ' -P ' + modeldir)
    W_attribute = np.load(file_name_W)

    # the resnet model
    model_file = os.path.join(modeldir, 'whole_wideresnet18_places365.pth.tar')
    if not os.access(model_file, os.W_OK):
        os.system('wget http://places2.csail.mit.edu/models_places365/whole_wideresnet18_places365.pth.tar -P ' + modeldir)

    if not os.access('wideresnet.py', os.W_OK):
        os.system('wget https://raw.githubusercontent.com/csailvision/places365/master/wideresnet.py')

    return classes, labels_IO, labels_attribute, W_attribute, model_file
