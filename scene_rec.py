import time

import cv2
import ipdb
import numpy as np
import os
import pandas as pd
import torch
from PIL import Image
from scipy.misc import imresize as imresize
from torch.autograd import Variable as V
from torch.nn import functional as F
from torchvision import transforms as trn

from model_dl import download_model


def hook_feature(module, input, output):
    features_blobs.append(np.squeeze(output.data.cpu().numpy()))


def returnCAM(feature_conv, weight_softmax, class_idx):
    # generate the class activation maps upsample to 256x256
    size_upsample = (256, 256)
    nc, h, w = feature_conv.shape
    output_cam = []
    for idx in class_idx:
        cam = weight_softmax[class_idx].dot(feature_conv.reshape((nc, h * w)))
        cam = cam.reshape(h, w)
        cam = cam - np.min(cam)
        cam_img = cam / np.max(cam)
        cam_img = np.uint8(255 * cam_img)
        output_cam.append(imresize(cam_img, size_upsample))
    return output_cam


def returnTF():
    # load the image transformer
    tf = trn.Compose([
        trn.Scale((224, 224)),
        trn.ToTensor(),
        trn.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ])
    return tf


def load_model(model_file, useGPU=1):
    if useGPU == 1:
        model = torch.load(model_file)
    else:
        model = torch.load(model_file, map_location=lambda storage, loc: storage)  # allow cpu

    model.eval()
    # hook the feature extractor
    features_names = ['layer4', 'avgpool']  # this is the last conv layer of the resnet
    for name in features_names:
        model._modules.get(name).register_forward_hook(hook_feature)
    return model


def image_list(image_root):
    paths = []
    image = []
    building = []
    tags = []
    for root, dirs, files in os.walk(image_root):
        for f in files:
            if f.endswith(".jpg"):
                path = os.path.join(root, f)
                paths.append(path)
                image.append(f.split('image_')[1][:-4])
                building.append(f.split('_')[1])
                tags.append(";".join(root.split(image_root)[1].split("/")))

    return pd.DataFrame({'image': image, 'building': building, 'paths': paths, 'tags': tags}, index=image)


GPUFLAG = 0

classes, labels_IO, labels_attribute, W_attribute, model_file = download_model()

features_blobs = []
features_names = ['layer4', 'avgpool']  # this is the last conv layer of the resnet
model = load_model(model_file)

# load the transformer
tf = returnTF()  # image transformer

# get the softmax weight
params = list(model.parameters())
weight_softmax = params[-2].data.numpy()
weight_softmax[weight_softmax < 0] = 0

# get images
import os, sys
print sys.platform, os.getlogin()
if sys.platform == 'darwin' and os.getlogin() == 'ashires':
    basedir = "/Users/ashires/data"
else:
    basedir = "/mnt/data"
input_dir = os.path.join(basedir, "gsv_comb_split_AF/")  # "/mnt/data/gsv_comb_split_KV/"
# input_dir = os.path.join(basedir, "projects","buildings","images","minc-2500","images","stone/")
input_dir = os.path.join(basedir, "images", "test", "input")
# use when local
# input_dir = "/Users/kvadakattu/Desktop"
print input_dir
df = image_list(input_dir)
print df.shape
out_root = os.path.join(basedir, "scene_rec/", input_dir.split('/')[-2])
# use when local
# out_root = "/Users/kvadakattu/Desktop/rbs"
if not os.path.exists(out_root):
    os.makedirs(out_root)
scene_categories = []
top_categories = []
top_category_scores = []
tnc = 5  # Top n categories
scene_attributes = []
top_attributes = []
top_attribute_scores = []
tna = 10  # Top n attributes

print("Processing %s images" % df.shape[0])
t0 = time.time()
for k, v in df.iterrows():
    print(v.image)
    img = Image.open(v.paths)
    input_img = V(tf(img).unsqueeze(0), volatile=True)

    # forward pass
    logit = model.forward(input_img)
    h_x = F.softmax(logit).data.squeeze()

    # output the prediction of scene category
    scene_categories.append(h_x)
    probs, idxc = h_x.sort(0, True)
    top_categories.append([classes[idxc[i]] for i in range(0, tnc)])
    top_category_scores.append(probs[:tnc])
    print '--SCENE CATEGORIES:'
    for i in range(0, tnc):
        print('{:.3f} -> {}'.format(probs[i], classes[idxc[i]]))

    # output the environment prediction
    io_image = np.mean(labels_IO[idxc[:10].numpy()])  # vote for indoor or outdoor
    df.loc[k, "environment"] = 'indoor' if io_image < 0.5 else 'outdoor'
    if io_image < 0.5:
        print '--TYPE OF ENVIRONMENT: indoor'
    else:
        print '--TYPE OF ENVIRONMENT: outdoor'

    # output the scene attributes
    responses_attribute = W_attribute.dot(features_blobs[-1])
    scene_attributes.append(responses_attribute)
    idx_a = np.argsort(responses_attribute)
    top_attributes.append([labels_attribute[idx_a[i]] for i in range(-1, -tna - 1, -1)])
    top_attribute_scores.append([responses_attribute[idx_a[i]] for i in range(-1, -tna - 1, -1)])
    print '--SCENE ATTRIBUTES:'
    print ', '.join(top_attributes[-1])

    # generate class activation mapping
    CAMs = returnCAM(features_blobs[-2], weight_softmax, [idxc[0]])
    # render the CAM and output
    img = cv2.imread(v.paths)
    height, width, _ = img.shape
    heatmap = cv2.applyColorMap(cv2.resize(CAMs[0], (width, height)), cv2.COLORMAP_JET)
    result = heatmap * 0.4 + img * 0.5
    save_dir = os.path.join(out_root, os.path.dirname(v.paths.split(input_dir)[1]))
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    cv2.imwrite(os.path.join(save_dir, v.image + '.jpg'), result)
    # use when local
    # cv2.imwrite(os.path.join(out_root, v.image + '.jpg'), result)

t_elapsed = time.time() - t0
sc = pd.DataFrame(scene_categories, columns=classes, index=df.index)
sa = pd.DataFrame(scene_attributes, columns=labels_attribute, index=df.index)
nc = pd.DataFrame(top_categories, columns=['cat_%s' % (1 + i) for i in range(tnc)], index=df.index)
ncs = pd.DataFrame(top_category_scores, columns=['cat_%s_score' % (1 + i) for i in range(tnc)], index=df.index)
na = pd.DataFrame(top_attributes, columns=['attr_%s' % (1 + i) for i in range(tna)], index=df.index)
nas = pd.DataFrame(top_attribute_scores, columns=['attr_%s_score' % (1 + i) for i in range(tna)], index=df.index)
output = pd.concat([df, nc, ncs, na, nas, sc, sa], axis=1, join='inner')
output.to_csv(os.path.join(out_root, input_dir.split('/')[-2] + '.csv'))
print("Processing Time for %s images: %s" % (output.shape[0], time.strftime("%M-%S", time.gmtime(t_elapsed))))
print("~%.2f secs per image" % (t_elapsed / output.shape[0]))
